#!/bin/bash

set -e

script_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
this="$script_dir/build2"
package_dir="$script_dir/sources"

container_name="lfs8-docker-container"
LFS=/mnt/lfs

main() {
    cd "$script_dir"

    if [[ $1 ]]; then
        # shellcheck disable=SC2145
        cmd_"$@"
    else
        get_sources
        build_it &> log-"$(date +%s)"
    fi
}

get_sources() {
    cd "$script_dir"
    if [[ -e sources ]]; then
        return
    fi
    mkdir -p sources
    wget --input-file="res/wget-list" --continue --directory-prefix="sources"
    pushd sources
    md5sum -c ../res/md5sums || die bad sources
    popd
}

build_it() {
    local args=(
        -it
        --privileged
        --name "$container_name"
        -v "$PWD:/lfs-source"
        debian:8
        bash /lfs-source/build2 start
    )
    docker run "${args[@]}"
}

cmd_start() {
    set -x
    local cmds
    cmds="$(declare -F | cut -d' ' -f3 | grep '^cmd_' | sort)"
    echo "cmds $cmds"

    cmd_1_as_root
    sudo -u lfs "$this" 2_as_user
}

cmd_1_as_root() {
    cmd_001_provision
    cmd_002_create_dirs_and_user
}

cmd_2_as_user() {
    cmd_003_usr_setup_user
    cmd_004_usr_binutils_pass1
    cmd_005_usr_gcc_pass1
}

cmd_001_provision() {
    apt-get -q update
    apt-get -q -y install build-essential bison gawk texinfo wget file sudo
    apt-get -q -y autoremove
    rm -rf /var/lib/apt/lists/*
    rm -f /bin/sh
    cd /bin
    ln -s bash sh
    mkdir -p /src "$LFS"
    chmod 777 /tmp
}

cmd_002_create_dirs_and_user() {
    mkdir -pv "$LFS/sources"
    chmod -v a+wt "$LFS/sources"
    mkdir -pv "$LFS/tools"
    ln -sv "$LFS/tools" /

    groupadd lfs
    useradd -s /bin/bash -g lfs -m -k /dev/null lfs

    echo lfs:lfs | chpasswd

    chown -v lfs "$LFS/tools" "$LFS/sources"
    chown lfs /src
}

cmd_003_usr_setup_user() {
    cat >~/.bash_profile <<'END'
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
END

    cat >~/.bashrc <<'END'
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin
export LFS LC_ALL LFS_TGT PATH
END
    export LC_ALL=POSIX
    export LFS_TGT=x86_64-lfs-linux-gnu
    export PATH=/tools/bin:/bin:/usr/bin
    export MAKEFLAGS="-j 8"
}

cmd_004_usr_binutils_pass1() {
    tar -xf "$package_dir"/binutils-* -C /tmp/
    cd /tmp/binutils-*
    mkdir -v build
    cd build
    ../configure \
        --prefix=/tools \
        --with-sysroot="$LFS" \
        --with-lib-path=/tools/lib \
        --target="$LFS_TGT" \
        --disable-nls \
        --disable-werror
    make
    mkdir -pv /tools/lib
    ln -sv lib /tools/lib64
    make install
    rm -rf /tmp/*
}

cmd_005_usr_gcc_pass1() {
    tar -xf "$package_dir"/gcc-*.tar.bz2 -C /tmp/

    cd /tmp/gcc-*

    tar -xf "$package_dir"/mpfr-*.tar.xz
    mv -v mpfr-* mpfr

    tar -xf "$package_dir"/gmp-*.tar.xz
    mv -v gmp-* gmp

    tar -xf "$package_dir"/mpc-*.tar.gz
    mv -v mpc-* mpc

    for file in gcc/config/{linux,i386/linux,i386/linux64}.h; do
        cp -uv "$file"{,.orig}
        sed -e 's@/lib\(64\)\?\(32\)\?/ld@/tools&@g' \
            -e 's@/usr@/tools@g' "$file".orig > "$file"
        echo '
#undef STANDARD_STARTFILE_PREFIX_1
#undef STANDARD_STARTFILE_PREFIX_2
#define STANDARD_STARTFILE_PREFIX_1 "/tools/lib/"
#define STANDARD_STARTFILE_PREFIX_2 ""' >> "$file"
        touch "$file".orig
    done

    sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64

    mkdir -v build

    cd build

    ../configure \
        --target="$LFS_TGT" \
        --prefix=/tools \
        --with-glibc-version=2.11 \
        --with-sysroot="$LFS" \
        --with-newlib \
        --without-headers \
        --with-local-prefix=/tools \
        --with-native-system-header-dir=/tools/include \
        --disable-nls \
        --disable-shared \
        --disable-multilib \
        --disable-decimal-float \
        --disable-threads \
        --disable-libatomic \
        --disable-libgomp \
        --disable-libmpx \
        --disable-libquadmath \
        --disable-libssp \
        --disable-libvtv \
        --disable-libstdcxx \
        --enable-languages=c,c++

    make
    make install

    rm -rf /tmp/*
}

main "$@"
